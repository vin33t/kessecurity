<?php

namespace Database\Seeders;

use App\Models\NatureOfComplaint;
use App\Models\ServiceClientType;
use App\Models\ServiceConcern;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class OptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $concerns = [
            'CCTV not Working',
            'VDP not Working',
            'Gate Monitor not Working',
            'Wifi not Working',
            'Security System not Working',
            'Beep Sound not Working',
            'View Issue',
            'Other',
        ];
        foreach($concerns as $concern){
            ServiceConcern::create([
               'option'=>$concern
            ]);
        }
        $clientTypes = [
            'Annual Maintenance Charges',
            'In Warranty',
            'PCB(Per Call Basis)'
        ];
        foreach($clientTypes as $type){
            ServiceClientType::create([
                'option'=>$type
            ]);
        }

        $natureOfComplaint = [
         'Breakdown',
         'Preventive Maintenance'
        ];
        foreach($natureOfComplaint as $nature){
            NatureOfComplaint::create([
                'option'=>$nature
            ]);
        }

    }
}
