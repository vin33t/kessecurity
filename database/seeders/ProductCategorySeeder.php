<?php

namespace Database\Seeders;

use App\Models\ProductCategories;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoryDetails = [['name'=>'CCTV Camera','image'=>'https://essezymb.sirv.com/cctv.png'],['name'=>'VDP','image'=>'https://essezymb.sirv.com/vdp.png'],['name'=>'Router','image'=>'https://essezymb.sirv.com/router.png'],['name'=>'Biometric','image'=>'https://essezymb.sirv.com/biometric.png']];
        foreach ($categoryDetails as $detail) {
            $category = ProductCategories::create([
                'name'=>$detail['name'],
            ]);
            $category->addMediaFromUrl($detail['image'])->toMediaCollection();

        }
    }
}
