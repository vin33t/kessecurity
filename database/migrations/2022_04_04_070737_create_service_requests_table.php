<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_requests', function (Blueprint $table) {
            $table->id();
            $table->string('requestId');
            $table->integer('userId');
            $table->integer('categoryId');
            $table->longText('serviceConcern');
            $table->longText('serviceClientType');
            $table->longText('natureOfComplaint');
            $table->integer('executiveId')->nullable();
            $table->schemalessAttributes('address');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_requests');
    }
};
