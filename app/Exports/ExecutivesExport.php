<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExecutivesExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function headings(): array
    {
        return ["Name", "Email", "Phone", 'Unique Executive Id'];
    }

    public function collection()
    {
        return User::select('name','email','phone','uniqueId')->where('type', 1)->get();
    }
}
