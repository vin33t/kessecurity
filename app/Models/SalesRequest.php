<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\SchemalessAttributes\Casts\SchemalessAttributes;
use Spatie\SchemalessAttributes\SchemalessAttributesTrait;

class SalesRequest extends Model
{
    use HasFactory, SchemalessAttributesTrait;

    protected $guarded = ['id'];

    public $casts = [
        'address' => SchemalessAttributes::class,
    ];

    public function scopeWithExtraAttributes(): Builder
    {
        return $this->address->modelScope();
    }
    public function category(){
        return $this->belongsTo('App\Models\ProductCategories','categoryId');
    }

    public function status(){
        return $this->hasMany('App\Models\SalesRequestStatus','requestId');
    }

  public function user(){
        return $this->belongsTo('App\Models\User','userId');
    }

}
