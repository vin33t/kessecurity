<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\SchemalessAttributes\Casts\SchemalessAttributes;
use Spatie\SchemalessAttributes\SchemalessAttributesTrait;

class ServiceRequest extends Model implements HasMedia
{
    use HasFactory, SchemalessAttributesTrait, InteractsWithMedia;

    protected $guarded = ['id'];

    public $casts = [
        'address' => SchemalessAttributes::class,
    ];

    public function scopeWithExtraAttributes(): Builder
    {
        return $this->address->modelScope();
    }

    public function category(){
        return $this->belongsTo('App\Models\ProductCategories','categoryId');
    }

    public function status(){
        return $this->hasMany('App\Models\ServiceRequestStatus','requestId');
    }
    public function user(){
        return $this->belongsTo('App\Models\User','userId');
    }
}
