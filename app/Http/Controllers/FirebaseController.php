<?php

namespace App\Http\Controllers;

use Google\Cloud\Firestore\FirestoreClient;
use Illuminate\Http\Request;
use Kreait\Firebase\Factory;

class FirebaseController extends Controller
{
    protected $database;

//    public function __construct()
//    {
//        $this->database = app('firebase.firestore');
//    }

    public function locations($executiveId)
    {
        $db = new FirestoreClient([
            'projectId' => 'securitygadgets-9ca81'
        ]);
        $usersRef = $db->collection('locations');
        $snapshot = $usersRef->document($executiveId)->snapshot();
        dd($snapshot['coordinates']);
//        foreach ($snapshot as $user) {
//            dd($user);
//            printf($user->coordinates);
//        }
//        $usersRef = $db->collection('locations/ex_    1/coordinates');
//        dd($usersRef);
    }

    public function send()
    {
        $notification_ids = ['asdasdjgjhasbdjhasyhdjgyuavsdgsdvajhgsad', 'wedcsfsdf'];
        $title = "Greeting Notification";
        $message = "Have good day!";
        $id = 1;
        $type = "basic";

        $res = $this->send_notification_FCM($notification_ids, $title, $message, $id, $type);

        if ($res == 1) {

            // success code
            dd('success');
        } else {
            dd('failure');

            // fail code
        }
    }


    public function sendNotification($tokens, $title, $body, $type = 'data')
    {
//        $firebaseToken = User::whereNotNull('device_token')->pluck('device_token')->all();

        $SERVER_API_KEY = env('FCM_KEY');

        $data = [
            "registration_ids" => $tokens,
            "data" => [
                "title" => $title,
                "body" => $body,
            ]
        ];

//        if ($type == 'notification') {
//            $data = [
//                "registration_ids" => $tokens,
//                "notification" => [
//                    "title" => $title,
//                    "body" => $body,
//                ],
//                "data" => [
//                    "title" => $title,
//                    "body" => $body,
//                ]
//            ];
//        } else {
//            $data = [
//                "registration_ids" => $tokens,
//                "data" => [
//                    "title" => $title,
//                    "body" => $body,
//                ]
//            ];
//        }
        $dataString = json_encode($data);

        $headers = [
            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

        $response = curl_exec($ch);
//        return $response;
        return back()->with('success', 'Notification send successfully.');
    }

    function send_notification_FCM($notification_ids, $title, $message, $requestId, $type)
    {

        $accesstoken = env('FCM_KEY');

        $URL = 'https://fcm.googleapis.com/fcm/send';


        $data = [
            "to" => $notification_ids,
            "notification" => [
                "body" => "' . $message . '",
                "title" => "' . $title . '",
                "type" => "' . $type . '",
                "id" => "' . $requestId . '",
                "message" => "' . $message . '",
                "icon" => "new",
                "sound" => "default"
            ],

        ];
        // print_r($post_data);die;
        $post_data = json_encode($data);
        $crl = curl_init();

        $headr = array();
        $headr[] = 'Content-type: application/json';
        $headr[] = 'Authorization: ' . $accesstoken;
        curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($crl, CURLOPT_URL, $URL);
        curl_setopt($crl, CURLOPT_HTTPHEADER, $headr);

        curl_setopt($crl, CURLOPT_POST, true);
        curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);

        $rest = curl_exec($crl);

        if ($rest === false) {
            // throw new Exception('Curl error: ' . curl_error($crl));
            //print_r('Curl error: ' . curl_error($crl));
            $result_noti = 0;
        } else {

            $result_noti = 1;
        }

        //curl_close($crl);
        //print_r($result_noti);die;
        return $result_noti;
    }
}
