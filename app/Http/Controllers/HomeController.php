<?php

namespace App\Http\Controllers;

use App\Exports\CustomerExport;
use App\Exports\ExecutivesExport;
use App\Models\Cities;
use App\Models\ProductCategories;
use App\Models\SalesRequest;
use App\Models\SalesRequestStatus;
use App\Models\ServiceRequest;
use App\Models\ServiceRequestStatus;
use App\Models\States;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function users($type)
    {
        if ($type == 'executive') {
            $users = User::where('type', 1)->get();
        } else {
            $users = User::where('type', 0)->get();
        }
            return view('users.list')->with(['users' => $users, 'type' => $type]);
    }

    public function requests($type){
        if ($type == 'sales') {
            $requests = SalesRequest::all();
        } else {
            $requests = ServiceRequest::all();
        }
        return view('requests.list')->with(['requests' => $requests, 'type' => $type]);
    }

    public function executiveTarget(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
           'executive'=>'required|exists:users,id',
           'target'=>'required|integer',
           'targetFor'=>'required|date'
        ]);
        $executive = User::find($request->executive);
        $executive->targets()->create([
            'targetFor'=>Carbon::parse($request->targetFor)->format('Y-m-d'),
            'target'=>$request->target
        ]);
        return redirect()->back();
    }

    public function categories(){
        $categories = ProductCategories::all();
        return view('categories')->with('categories',$categories);
    }

    public function categoriesAdd(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
           'categoryName' => 'required|string',
           'categoryImage' => 'required|image|mimes:jpg,png,jpeg,gif,svg'
        ]);
//        return $request;
        $newCategory = ProductCategories::create([
            'name'=>$request->categoryName
        ]);
        $newCategory->addMedia($request->categoryImage)->toMediaCollection();
        return redirect()->back();
    }

    public function export($type){
        if($type == 'executive'){
            return Excel::download(new ExecutivesExport, 'executives.xlsx');
        } elseif($type == 'customer'){
            return Excel::download(new CustomerExport, 'customers.xlsx');
        }
        return null;
    }


    public function serviceableArea(){
        $states = States::all();
        $serviceableCities = Cities::all();
        return view('serviceableArea')->with([
            'states'=>$states,
            'serviceableCities'=>$serviceableCities
        ]);
    }

    public function addServiceableCity(Request $request){
        $request->validate([
            'state'=>'required|exists:states,id',
            'cityName'=>'required|string',
            'cityPincode'=>'required|integer'
        ]);

        $city = Cities::create([
           'stateId'=>$request->state,
           'name'=>$request->cityName,
           'pincode'=>$request->cityPincode
        ]);
        return redirect()->back();
    }
}
