<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FirebaseController;
use App\Models\NatureOfComplaint;
use App\Models\ProductCategories;
use App\Models\SalesRequest;
use App\Models\ServiceClientType;
use App\Models\ServiceConcern;
use App\Models\ServiceRequest;
use App\Models\States;
use App\Models\User;
use App\Traits\ApiResponse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ApiController extends Controller
{
    use ApiResponse;

    public function requestDetails(Request $request)
    {
        $request->validate([
            'requestId' => 'required'
        ]);
        $type = Str::contains($request->requestId, 'SER') ? 'service' : 'sales';
        $vRule = $type == 'service' ? '' : 'exists:sales_requests,requestId';
        $request->validate([
            'requestId' => $vRule,
        ]);
        $searchedRequest = $type == 'service' ? ServiceRequest::where('requestId', $request->requestId)->first() : SalesRequest::where('requestId', $request->requestId)->first();
        $requestStatus = $this->requestStatus($searchedRequest);
        $result = [
            'requestId' => $searchedRequest->requestId,
            'category' => $searchedRequest->category->name,
            'address' => $searchedRequest->address,
            'status' => $requestStatus[0],
            'statusDetails' => $requestStatus[1],
            'customerName' => User::find($searchedRequest->userId)->name,
            'executiveName' => $requestStatus[2],
            'executiveId' => $requestStatus[3],
        ];
        if ($request->type == 'service') {
            $result['serviceConcern'] = $searchedRequest->serviceConcern;
            $result['serviceClientType'] = $searchedRequest->serviceClientType;
            $result['natureOfComplaint'] = $searchedRequest->natureOfComplaint;
        }

        return $result;
    }

    public function updateUser(Request $request)
    {
        $request->validate([
            'name' => 'string',
            'phone' => 'integer|digits:10',
            'email' => 'unique:users,email'
        ]);
        $request->name ? auth()->user()->update(['name' => $request->name]) : null;
        $request->phone ? auth()->user()->update(['phone' => $request->phone]) : null;
        $request->email ? auth()->user()->update(['email' => $request->email]) : null;
        $user = auth()->user();
        $userDetails = [
            'name' => $user->name,
            'email' => $user->email,
            'phone' => $user->phone,
            'type' => $user->type == 0 ? 'user' : 'executive'
        ];
        return $this->success($userDetails, 'Details Updated');

    }

    public function user()
    {
        $user = auth()->user();
        $userDetails = [
            'name' => $user->name,
            'email' => $user->email,
            'phone' => $user->phone,
            'type' => $user->type == 0 ? 'user' : 'executive'
        ];
        $user->type == 1 ? $userDetails['status'] = ($user->status ? 'online' : 'offline') : null;
        return $this->success($userDetails, 'User Details');

    }

    public function dropdowns()
    {
        $options = collect();
        $serviceConcerns = collect();
        $serviceClientTypes = collect();
        $natureOfComplaints = collect();

        foreach (ServiceConcern::all() as $serviceConcern) {
            $serviceConcerns->push($serviceConcern->option);
        }
        foreach (ServiceClientType::all() as $serviceClientType) {
            $serviceClientTypes->push($serviceClientType->option);
        }
        foreach (NatureOfComplaint::all() as $natureOfComplaint) {
            $natureOfComplaints->push($natureOfComplaint->option);
        }
        $options->push([
            'serviceConcerns' => $serviceConcerns,
            'clientTypes' => $serviceClientTypes,
            'natureOfComplaints' => $natureOfComplaints,
        ]);
        return $this->success($options, 'Dropdown Options');

    }

    public function categories()
    {
        $categories = collect();
        foreach (ProductCategories::all() as $category) {
            $categories->push([
                'id' => $category->id,
                'name' => $category->name,
                'image' => $category->getFirstMediaUrl(),
            ]);
        }
        return $this->success($categories, 'Categories');
    }

    public function serviceRequest(Request $request)
    {
        $service = $request->validate([
            'categoryId' => 'required|integer|exists:product_categories,id',
            'serviceConcern' => 'required|string',
            'serviceClientType' => 'required|string',
            'natureOfComplaint' => 'required|string',
            'houseNumber' => 'required|string',
            'floor' => 'string',
            'nearestLandmark' => 'required|string',
            'city' => 'required|string',
            'postalCode' => 'required|integer',
            'phone' => 'required|integer|digits:10',
            'documents' => 'required',
            'lat' => 'required|string',
            'long' => 'required|string',
//            'files.*'=>'required|mimes:jpeg,bmp,png,gif,svg,pdf'
        ]);
        $latestServiceRequest = ServiceRequest::orderBy('created_at', 'DESC');
        if ($latestServiceRequest->count()) {
            $latestServiceRequest = $latestServiceRequest->first();
            $requestId = 'SER' . Carbon::now()->format('ym') . str_pad($latestServiceRequest->id + 1, 8, "0", STR_PAD_LEFT);
        } else {
            $requestId = 'SER' . Carbon::now()->format('ym') . str_pad(0 + 1, 8, "0", STR_PAD_LEFT);
        }
        $serviceRequest = new ServiceRequest();
        $serviceRequest->requestId = $requestId;
        $serviceRequest->userId = auth()->user()->id;
        $serviceRequest->categoryId = $service['categoryId'];
        $serviceRequest->serviceConcern = $service['serviceConcern'];
        $serviceRequest->serviceClientType = $service['serviceClientType'];
        $serviceRequest->natureOfComplaint = $service['natureOfComplaint'];
        $serviceRequest->address['houseNumber'] = $service['houseNumber'];
        $request->floor ? $serviceRequest->address['floor'] = $service['floor'] : null;
        $serviceRequest->address['nearestLandmark'] = $service['nearestLandmark'];
        $serviceRequest->address['city'] = $service['city'];
        $serviceRequest->address['postalCode'] = $service['postalCode'];
        $serviceRequest->address['phone'] = $service['phone'];
        $serviceRequest->address['lat'] = $service['lat'];
        $serviceRequest->address['long'] = $service['long'];
        $serviceRequest->save();
        $serviceRequest->status()->create();

        foreach ($request->documents as $document) {
            $serviceRequest->addMedia($document)->toMediaCollection();
        }
        $notification_ids = User::where('type', 1)->where('fcmToken', '!=', null)->pluck('fcmToken');
        $title = "New Service Request";
        $message = $requestId;

        (new FirebaseController)->sendNotification($notification_ids, $title, $message);
        return $this->success('', 'Service Request Created');
    }


    public function salesRequest(Request $request)
    {
        $sale = $request->validate([
            'categoryId' => 'required|integer|exists:product_categories,id',
            'houseNumber' => 'required|string',
            'floor' => 'string',
            'nearestLandmark' => 'required|string',
            'city' => 'required|string',
            'postalCode' => 'required|integer',
            'phone' => 'required|integer|digits:10',
            'lat' => 'required|string',
            'long' => 'required|string',
        ]);


        $latestSalesRequest = SalesRequest::orderBy('created_at', 'DESC');
        if ($latestSalesRequest->count()) {
            $latestSalesRequest = $latestSalesRequest->first();
            $requestId = 'SAR' . Carbon::now()->format('ym') . str_pad($latestSalesRequest->id + 1, 8, "0", STR_PAD_LEFT);
        } else {
            $requestId = 'SAR' . Carbon::now()->format('ym') . str_pad(0 + 1, 8, "0", STR_PAD_LEFT);
        }
        $saleRequest = new SalesRequest();
        $saleRequest->userId = auth()->user()->id;
        $saleRequest->requestId = $requestId;
        $saleRequest->categoryId = $sale['categoryId'];
        $saleRequest->address['houseNumber'] = $sale['houseNumber'];
        $request->floor ? $saleRequest->address['floor'] = $sale['floor'] : null;
        $saleRequest->address['nearestLandmark'] = $sale['nearestLandmark'];
        $saleRequest->address['city'] = $sale['city'];
        $saleRequest->address['postalCode'] = $sale['postalCode'];
        $saleRequest->address['phone'] = $sale['phone'];
        $saleRequest->address['lat'] = $sale['lat'];
        $saleRequest->address['long'] = $sale['long'];
        $saleRequest->save();
        $saleRequest->status()->create();

        $notification_ids = User::where('type', 1)->where('fcmToken', '!=', null)->pluck('fcmToken');
        $title = "New Service Request";
        $message = $requestId;
        (new FirebaseController)->sendNotification($notification_ids, $title, $message);

        return $this->success('', 'Sales Request Created');
    }

    public function serviceRequestList(Request $request)
    {
//        return $request;
        $request->validate([
            'filterFromDate' => 'date',
            'filterToDate' => 'date',
        ]);

        if (auth()->user()->type == 1) {
            $serviceRequests = ServiceRequest::where('id', '!=', 0);
        } else {

            $serviceRequests = ServiceRequest::where('userId', auth()->user()->id);
        }

        if ($request->filterFromDate && $request->filterToDate) {
            $from = Carbon::parse($request->filterFromDate)->toDateString();
            $to = Carbon::parse($request->filterToDate)->toDateString();
            $serviceRequests = $serviceRequests->whereBetween('created_at', [$from, $to]);
        } elseif ($request->filterFromDate) {
            $from = Carbon::parse($request->filterFromDate)->toDateString();
            $serviceRequests = $serviceRequests->where('created_at', '>=', $from);

        }
        $data = collect();
        foreach ($serviceRequests->get() as $sRequest) {
            $status = $this->requestStatus($sRequest);

            $result = [
                'requestId' => $sRequest->requestId,
                'category' => $sRequest->category->name,
                'serviceConcern' => $sRequest->serviceConcern,
                'serviceClientType' => $sRequest->serviceClientType,
                'natureOfComplaint' => $sRequest->natureOfComplaint,
                'address' => $sRequest->address,
                'status' => $status[0],
                'statusDetails' => $status[1],
                'created_at' => $sRequest->created_at
            ];
            if ($request->filter) {
                if ($request->filter == $status[0]) {
                    $data->push($result);
                }
                if($request->filter == 'ongoing'){
                    if($status[0] == 'followUp'){
                        $data->push($result);
                    }
                }
            } else {
                $data->push($result);
            }
        }
        return $this->success($data, 'Service Request List');
    }

    public function salesRequestList(Request $request)
    {
        $request->validate([
            'filterFromDate' => 'date',
            'filterToDate' => 'date',
        ]);
        if (auth()->user()->type == 1) {
            $salesRequest = SalesRequest::where('id', '!=', 0);
        } else {
            $salesRequest = SalesRequest::where('userId', auth()->user()->id);

        }
        if ($request->filterFromDate && $request->filterToDate) {
            $from = Carbon::parse($request->filterFromDate)->toDateString();
            $to = Carbon::parse($request->filterToDate)->toDateString();
            $salesRequest = $salesRequest->whereBetween('created_at', [$from, $to]);
        } elseif ($request->filterFromDate) {
            $from = Carbon::parse($request->filterFromDate)->toDateString();
            $salesRequest = $salesRequest->where('created_at', '>=', $from);

        }
        $data = collect();
        foreach ($salesRequest->get() as $sRequest) {

            $status = $this->requestStatus($sRequest);
            $result = [
                'requestId' => $sRequest->requestId,
                'category' => $sRequest->category->name,
                'address' => $sRequest->address,
                'status' => $status[0],
                'statusDetails' => $status[1],
                'created_at' => $sRequest->created_at
            ];

            if ($request->filter) {
                if ($request->filter == $status[0]) {
                    $data->push($result);
                }
                if($request->filter == 'ongoing'){
                    if($status[0] == 'followUp'){
                        $data->push($result);
                    }
                }
            } else {
                $data->push($result);
            }
        }
        return $this->success($data, 'Sales Request List');
    }


    public function requestStatus($sRequest)
    {
        $sStatus = $sRequest->status()->orderBy('created_at', 'DESC')->first();
        if ($sStatus->status == 0) {
            $status = 'new';
            $statusDetails = 'Active but not accepted by any executive';
            $statusExecutiveName = null;
            $statusExecutiveId = null;
        } elseif ($sStatus->status == 1) {
            $status = 'ongoing';
            $statusDetails = 'Assigned to ' . User::find($sStatus->executiveId)->name;
            $statusExecutiveName =  User::find($sStatus->executiveId)->name;
            $statusExecutiveId = User::find($sStatus->executiveId)->uniqueId;


        } elseif ($sStatus->status == 2) {
            $status = 'followUp';
            $statusDetails = 'Follow Up for ' . Carbon::parse($sRequest->followUp)->format('d F, Y h:i A');
            $statusExecutiveName = User::find($sStatus->executiveId)->name;;
            $statusExecutiveId = User::find($sStatus->executiveId)->uniqueId;;


        } elseif ($sStatus->status == 3) {
            $status = 'completed';
            $statusDetails = 'Request Completed';
            $statusExecutiveName =  User::find($sStatus->executiveId)->name;;
            $statusExecutiveId = User::find($sStatus->executiveId)->uniqueId;


        } elseif ($sStatus->status == 4) {
            $status = 'rejected';
            $statusDetails = 'Request Rejected';
            $statusExecutiveName =  User::find($sStatus->executiveId)->name;
            $statusExecutiveId = User::find($sStatus->executiveId)->uniqueId;


        }
        return [$status, $statusDetails,$statusExecutiveName, $statusExecutiveId];
    }


    public function serviceableStates(){
        $states = States::has('cities')->get()->map(function($state){
            unset($state->created_at);
            unset($state->updated_at);
            return $state;
        });
        return $this->success($states, 'Serviceable States');
    }

    public function serviceableCities(Request $request){
        $request->validate([
            'stateId'=>'required|exists:states,id'
        ]);
        $state = States::find($request->stateId);
        $cities = States::find($request->stateId)->cities()->get()->map(function($city){
            unset($city->created_at);
            unset($city->updated_at);
            unset($city->id);
            unset($city->stateId);
            return $city;
        });
        return $this->success($cities->count() ? $cities : null, $cities->count() ? $cities->count() . ' Serviceable Cities in ' . $state->name : 'No Serviceable Cities in ' . $state->name);
    }
}
