<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Traits\ApiResponse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    use ApiResponse;

    public function register(Request $request)
    {
        $attr = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|unique:users,email',
            'phone' => 'required|integer|digits:10|unique:users,phone',
            'password' => 'required|string|min:6',
            'type' => 'required|in:user,executive',
            'fcmToken'=>'required|string'
        ]);

        $user = User::create([
            'name' => $attr['name'],
            'password' => bcrypt($attr['password']),
            'phone' => $attr['phone'],
            'email' => $attr['email'],
            'type' => $attr['type'] == 'user' ? 0 : 1,
            'fcmToken' => $attr['fcmToken']
        ]);
        $users = User::where('type',1)->orderBy('created_at', 'DESC');
        if ($users->count()) {
            $us = $users->first();
            $userId = 'EX' . Carbon::now()->format('m') . str_pad($us->id + 1, 8, "0", STR_PAD_LEFT);
        } else {
            $userId = 'EX' . Carbon::now()->format('m') . str_pad(0 + 1, 8, "0", STR_PAD_LEFT);
        }
        $user->type = 1 ? $user->update(['uniqueId'=>$userId]) : null;

        $usr = $user;
        $usr['type'] = $usr->type == 0 ? 'user' : 'executive';
        $usr->type == 1 ? $usr['uniqueId'] = $usr->uniqueId : null;

        return $this->success([
            'user' => $usr,
            'token' => $user->createToken('API Token')->plainTextToken
        ], 'User Registered');
    }

    public function login(Request $request)
    {
        $attr = $request->validate([
            'email' => 'required|string|email|',
            'password' => 'required|string|min:6',
//            'fcmToken' => 'required|string'
        ]);
        $request->validate([
            'fcmToken' => 'required|string'

        ]);
//        return $request;
        if (!Auth::attempt($attr)) {
            return $this->error('Credentials do not match', 401);
        }
        $user = auth()->user();
        $user->update(['fcmToken' => $request->fcmToken]);
        $user['type'] = $user->type == 0 ? 'user' : 'executive';
        $user->type == 1 ? $user['uniqueId'] = $user->uniqueId : null;
        return $this->success([
            'user' => $user,
            'token' => auth()->user()->createToken('API Token')->plainTextToken
        ], 'Logged In');
    }

    public function logout()
    {
        auth()->user()->tokens()->delete();

        return [
            'message' => 'Tokens Revoked'
        ];
    }
}
