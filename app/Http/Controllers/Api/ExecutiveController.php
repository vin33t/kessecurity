<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\FirebaseController;
use App\Models\SalesRequest;
use App\Models\SalesRequestStatus;
use App\Models\ServiceRequest;
use App\Models\ServiceRequestStatus;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponse;
use Illuminate\Support\Str;


class ExecutiveController extends Controller
{
    use ApiResponse;

    public function updateRequest(Request $request)
    {
//        return $request;
        $request->validate([
            'requestId' => 'required',
            'status' => 'required|in:new,ongoing,followUp,completed,rejected',
            'media' => 'required|file|mimes:jpg,png,jpeg,gif,svg,mp4,mov,ogg,qt',
            'remarks' => 'string',
            'followUp' => 'date'
        ]);
        $status = ['new' => 0, 'ongoing' => 1, 'followUp' => 2, 'completed' => 3, 'rejected' => 4];
//        SAR => Sales Request, SER => Service Request
        $vRule = Str::contains($request->requestId, 'SER') ? 'exists:service_requests,requestId' : 'exists:sales_requests,requestId';
        $request->validate([
            'requestId' => $vRule,
        ]);
        $inputRequest = Str::contains($request->requestId, 'SER') ? ServiceRequest::where('requestId', $request->requestId)->first() : SalesRequest::where('requestId', $request->requestId)->first();
        $st = $inputRequest->status()->create([
            'executiveId'=>auth()->user()->id,
            'status'=>$status[$request->status]
        ]);

        $request->remarks ? $st->remarks = $request->remarks : null;
        $request->followUp ? $st->followUp = $request->followUp : null;

        $st->addMedia($request->media)->toMediaCollection();

        return $this->success(null, 'Status Updated');

    }

    public function updateStatus(Request $request)
    {
        $request->validate([
            'status' => 'required|boolean'
        ]);
        auth()->user()->update([
            'status' => $request->status
        ]);
        return $this->success(auth()->user()->status ? 'online' : 'offline', 'Status Updated');

    }

    public function myTarget(Request $request)
    {
        $request->validate([
            'filterMonth' => 'date'
        ]);
        $user = auth()->user();
        $request->filterMonth ? $selectedMonth = Carbon::parse($request->filterMonth)->startOfMonth()->format('Y-m-d') : $selectedMonth = Carbon::now()->startOfMonth()->format('Y-m-d');
        $t = $user->targets()->where('targetFor', $selectedMonth);
        if ($t->count()) {
            $t = $t->first();
            $target['target'] = $t->target;
            $target['achieved'] = 0;
            $target['month'] = Carbon::parse($t->targetFor)->format('F Y');
        } else {
            $target = null;
        }
        return $this->success($target, 'Target Not Available for this month');

    }

    public function acceptRequest(Request $request)
    {
        $request->validate([
            'requestId' => 'required',
            'accept' => 'required|boolean',
        ]);

//        SAR => Sales Request, SER => Service Request
        $vRule = Str::contains($request->requestId, 'SER') ? 'exists:service_requests,requestId' : 'exists:sales_requests,requestId';
        $request->validate([
            'requestId' => $vRule,
        ]);
        $inputRequest = Str::contains($request->requestId, 'SER') ? ServiceRequest::where('requestId', $request->requestId)->first() : SalesRequest::where('requestId', $request->requestId)->first();
        if ($request->accept == 1) {
//            return $inputRequest->status()->where('status', 0)->where('executiveId', '!=', null)->get();
            if ($inputRequest->status()->where('status', 0)->where('executiveId', null)->count()) {
                $inputRequest->executiveId = auth()->user()->id;
                $inputRequest->status()->create([
                    'executiveId' => auth()->user()->id,
                    'status' => 1
                ]);
                $notification_ids = array(User::find($inputRequest->userId)->fcmToken);
                $title = "Request Accepted";
                $message =['requestId'=>$inputRequest->requestId, 'executiveId',User::find($inputRequest->executiveId)->uniqueId];
                (new FirebaseController)->sendNotification($notification_ids, $title, $message, 'notification');
                return $this->success(null, $request->requestId . ' Request Accepted');
            } else {
                return $this->error($request->requestId . ' Already accepted by other executive', 401);
            }


        } else {
            $inputRequest->status()->create([
                'executiveId' => auth()->user()->id,
                'status' => 4
            ]);
            return $this->success(null, $request->requestId . ' Request Rejected');
        }

//        return $inputRequest;

    }

    public function myStats(){
        $salesRequestsAccepted = SalesRequestStatus::whereDate('created_at',Carbon::now())->where('executiveId', auth()->user()->id)->where('status',1)->orderBy('created_at','DESC')->get()->unique('requestId');
        $serviceRequestsAccepted = ServiceRequestStatus::whereDate('created_at',Carbon::now())->where('executiveId', auth()->user()->id)->where('status',1)->orderBy('created_at','DESC')->get()->unique('requestId');

        $salesRequestsRejected = SalesRequestStatus::whereDate('created_at',Carbon::now())->where('executiveId', auth()->user()->id)->where('status',4)->orderBy('created_at','DESC')->get()->unique('requestId');
        $serviceRequestsRejected = ServiceRequestStatus::whereDate('created_at',Carbon::now())->where('executiveId', auth()->user()->id)->where('status',4)->orderBy('created_at','DESC')->get()->unique('requestId');

        $salesRequestsCompleted = SalesRequestStatus::where('executiveId', auth()->user()->id)->where('status',3)->orderBy('created_at','DESC')->get()->unique('requestId');
        $serviceRequestsCompleted = ServiceRequestStatus::where('executiveId', auth()->user()->id)->where('status',3)->orderBy('created_at','DESC')->get()->unique('requestId');

        $salesRequestsFollowups = SalesRequestStatus::where('executiveId', auth()->user()->id)->where('status',2)->orderBy('created_at','DESC')->get()->unique('requestId');
        $serviceRequestsFollowups = ServiceRequestStatus::where('executiveId', auth()->user()->id)->where('status',2)->orderBy('created_at','DESC')->get()->unique('requestId');

        $salesRequestsInprogress = SalesRequestStatus::where('executiveId', auth()->user()->id)->where('status', 1)->orderBy('created_at', 'DESC')->get()->unique('requestId');
        $serviceRequestsInprogress = ServiceRequestStatus::where('executiveId', auth()->user()->id)->where('status', 1)->orderBy('created_at', 'DESC')->get()->unique('requestId');


        $todayAccepted = $salesRequestsAccepted->count() + $serviceRequestsAccepted->count();
        $todayRejected = $salesRequestsRejected->count() + $serviceRequestsRejected->count();
        return $this->success([
            'requestsAcceptedToday'=>$todayAccepted,
            'requestsRejectedToday'=>$todayRejected,

            'totalRequestsToday'=>$todayAccepted + $todayRejected,

            'salesRequestsCompleted' => $salesRequestsCompleted->count(),
            'serviceRequestsCompleted' => $serviceRequestsCompleted->count(),

            'salesRequestsFollowups' => $salesRequestsFollowups->count(),
            'serviceRequestsFollowups' => $serviceRequestsFollowups->count(),

            'salesRequestsInprogress' => $salesRequestsInprogress->count(),
            'serviceRequestsInprogress' => $serviceRequestsInprogress->count(),


        ],
            auth()->user()->name . ' ' . 'Stats');

    }
}
