<?php

use App\Http\Controllers\NotificationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

//Auth::routes();
Route::get('/privacy-policy', function () {
    return view('privacyPolicy');
});

Route::get('/locations/{executiveId}',[\App\Http\Controllers\FirebaseController::class,'locations']);
Route::get('/send',[\App\Http\Controllers\FirebaseController::class,'send']);

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::get('push-notification', [NotificationController::class, 'index']);
Route::post('sendNotification', [NotificationController::class, 'sendNotification'])->name('send.notification');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/users/{type}', [App\Http\Controllers\HomeController::class, 'users'])->name('users');
Route::get('/request/{type}', [App\Http\Controllers\HomeController::class, 'requests'])->name('requests');
Route::post('/executive/target', [\App\Http\Controllers\HomeController::class,'executiveTarget'])->name('executive.target');
Route::get('/categories', [\App\Http\Controllers\HomeController::class,'categories'])->name('categories');
Route::post('/category/add', [\App\Http\Controllers\HomeController::class,'categoriesAdd'])->name('category.add');
Route::get('/export/{type}', [\App\Http\Controllers\HomeController::class,'export'])->name('export');
Route::get('/serviceable-area', [\App\Http\Controllers\HomeController::class,'serviceableArea'])->name('serviceable-area');
Route::post('/serviceable/city/add', [\App\Http\Controllers\HomeController::class,'addServiceableCity'])->name('serviceable.city.add');
