<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Api\ExecutiveController;

Route::group(['prefix' => 'auth'], function () {
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/login', [AuthController::class, 'login']);
});


    Route::get('/serviceableStates',[ApiController::class, 'serviceableStates']);
    Route::post('/serviceableCities',[ApiController::class, 'serviceableCities']);
Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('/user', [ApiController::class, 'user']);
    Route::post('/user/update', [ApiController::class, 'updateUser']);
    Route::get('/categories', [ApiController::class, 'categories']);
    Route::get('/dropdowns', [ApiController::class, 'dropdowns']);
    Route::post('/auth/logout', [AuthController::class, 'logout']);

    Route::group(['prefix' => 'request'], function () {
        Route::post('/details', [ApiController::class, 'requestDetails']);
        Route::group(['prefix' => 'list'], function () {
            Route::post('/sales', [ApiController::class, 'salesRequestList']);
            Route::post('/service', [ApiController::class, 'serviceRequestList']);
        });
        Route::post('/sales', [ApiController::class, 'salesRequest']);
        Route::post('/service', [ApiController::class, 'serviceRequest']);
    });


    Route::group(['prefix' => 'executive'], function () {
        Route::post('/target', [ExecutiveController::class, 'myTarget']);
        Route::get('/stats', [ExecutiveController::class, 'myStats']);
        Route::group(['prefix' => 'request'], function () {
            Route::post('/accept', [ExecutiveController::class, 'acceptRequest']);
            Route::post('/update', [ExecutiveController::class, 'updateRequest']);
        });
        Route::group(['prefix' => 'profile'], function () {
            Route::group(['prefix' => 'update'], function () {
                Route::post('/status', [ExecutiveController::class, 'updateStatus']);
            });
        });
    });
});
///opt/alt/php81/usr/bin/php artisan clear-compiled
///opt/alt/php81/usr/bin/php composer.phar dump-autoload
///opt/alt/php81/usr/bin/php artisan optimize
///opt/alt/php81/usr/bin/php artisan migrate:fresh --seed
