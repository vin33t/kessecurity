@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>{{ strtoupper($type) }}</h1>
@stop

<script>
    function filterKey() {
alert(1);
    }
</script>

@section('content')
    <p>Requests List</p>
    <div class="card">
        <div class="card-header">
            <div class="p-2">
                <p class="btn btn-outline-primary float-start">{{ $requests->count() }} Requests</p>
{{--                <p class="float-right">--}}
{{--                    <label>Filter</label>--}}
{{--                    <select name="filterKey" id="filterKey" class="form-control">--}}
{{--                        <option value="">All</option>--}}
{{--                        <option value="new">New</option>--}}
{{--                        <option value="ongoing">Ongoing</option>--}}
{{--                        <option value="followUp">Follow Up</option>--}}
{{--                        <option value="completed">Completed</option>--}}
{{--                        <option value="rejected">Rejected</option>--}}
{{--                    </select>--}}

{{--                </p>--}}
                <br />
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover" id="requestsTable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Request Id</th>
                        <th>Request By</th>
                        <th>Category</th>
                        <th>Address</th>

                        @if($type == 'service')
                            <th>Concern</th>
                            <th>Type</th>
                            <th>Nature</th>
                        @endif
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($requests as $request)
                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td>{{ $request->requestId }}</td>
                            <td>Name: {{ $request->user->name }} <br>
                                Phone: {{ $request->user->phone }}</td>
                            <td>{{ $request->category->name }}</td>
                            <td>{{ $request->address->houseNumber }}, {{ $request->address->nearestLandamrk }}
                                , {{ $request->address->city }}, {{ $request->address->postalCode }}</td>
                            @if($type == 'service')
                                <td>{{ $request->serviceConcern }}</td>
                                <td>{{ $request->serviceClientType }}</td>
                                <td>{{ $request->natureOfComplaint }}</td>
                            @endif
                            <td>
                                {{ strtoupper(app(\App\Http\Controllers\Api\ApiController::class)->requestStatus($request)[0]) }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
{{--                {{ $requests->links() }}--}}
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css"/>
@stop

@section('js')
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>


    <script async defer>
        $(function() {
            var table =  $('#requestsTable').DataTable({
                buttons: [
                    'copy', 'excel', 'pdf'
                ]
            });

            new $.fn.dataTable.Buttons( table, {
                buttons: [
                    'copy', 'excel', 'pdf'
                ]
            } );
            $("#filterKey").change(function() {
                alert( $('option:selected', this).val() );
            });
        });


    </script>
@stop
