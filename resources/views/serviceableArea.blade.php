@extends('adminlte::page')

@section('title', 'Categories')

@section('content_header')
    <h1>Serviceable Cities</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
                        {{ $serviceableCities->count() }} Cities
            <button class="btn btn-primary" data-toggle="modal" data-target="#addCategory">Add City</button>
            <div id="addCategory" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <form action="{{ route('serviceable.city.add') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                                <p>Add New Category</p>
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="state">State</label>
                                        <select name="state" id="state" class="form-control" required>
                                            <option value="">--Select State--</option>
                                            @foreach($states as $state)
                                                <option value="{{ $state->id }}">{{ $state->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">City Name</label>
                                        <input type="text" class="form-control" name="cityName" required>
                                    </div> <div class="col-md-6">
                                        <label for="">City Pincode</label>
                                        <input type="text" class="form-control" name="cityPincode" required>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Add City</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>State</th>
                        <th>City</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($serviceableCities as $city)
                        <tr>
                            <td>{{ $city->state->name }}</td>
                            <td>{{ $city->name }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop


@section('css')
@stop

@section('js')

@stop
