@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>{{ strtoupper($type) }}</h1>
@stop

@section('content')
    <p>Users List</p>
    <div class="card">
        <div class="card-header">
            <h5>{{ $users->count() }} Users
                @if($type == 'executive')
                    <a href="{{ route('export',['type'=>'executive']) }}">
                        @else
                            <a href="{{ route('export',['type'=>'customer']) }}">
                                @endif
                                <button class="btn btn-warning btn-sm">Export to Excel</button>
                            </a>

            </h5>
            @if($type == 'executive')
                <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Assign Sales Target</button>
                <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title"></h4>
                            </div>
                            <form action="{{ route('executive.target') }}" method="POST">
                                @csrf
                                <div class="modal-body">
                                    <p>Add Sales Target</p>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="">Executive</label>
                                            <select name="executive" id="executive" class="form-control" required>
                                                <option value="">--Select Executive--</option>
                                                @foreach(\App\Models\User::where('type',1)->get() as $user)
                                                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="">Target For</label>
                                            <select name="targetFor" id="targetFor" class="form-control" required>
                                                <option value="">--Month--</option>
                                                @foreach([ 'January','February','March','April','May','June','July ','August','September','October','November','December'] as $month)
                                                    @php
                                                        $targetFor = \Carbon\Carbon::now()->format('Y') . '-' . $loop->index + 1 . '-01';
                                                    @endphp
                                                    <option value="{{ $targetFor }}">{{ $month }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="">Target</label>
                                            <input type="number" class="form-control" name="target"
                                                   placeholder="Target" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Assign Target</button>
                                </div>
                        </div>
                        </form>

                    </div>
                </div>
            @endif
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover" id="usersTable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Email</th>
                        @if($type == 'executive')
                            <th>Target</th>
                            <th>Today's Stats</th>
                            <th>Stats</th>
                        @endif
                        @if($type == 'customers')
                            <th>Sales Requests</th>
                            <th>Service Requests</th>
                        @endif
                        {{--                        <th>--}}
                        {{--                            Login Status--}}
                        {{--                        </th>--}}
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->phone }}</td>
                            <td>{{ $user->email }}</td>
                            @if($type == 'executive')
                                <td>
                                    @foreach($user->targets as $target)
                                        Target For <strong><u>{{ $target->targetFor }}</u></strong>:
                                        <strong>{{ $target->target }}</strong>
                                        <br>
                                        Achieved: <b>0</b>
                                    @endforeach
                                </td>
                                @php
                                    $salesRequestsAccepted = App\Models\SalesRequestStatus::whereDate('created_at', \Carbon\Carbon::now())->where('executiveId', $user->id)->where('status', 1)->orderBy('created_at', 'DESC')->get()->unique('requestId');
                                   $serviceRequestsAccepted = App\Models\ServiceRequestStatus::whereDate('created_at', \Carbon\Carbon::now())->where('executiveId', $user->id)->where('status', 1)->orderBy('created_at', 'DESC')->get()->unique('requestId');

                                   $salesRequestsRejected = App\Models\SalesRequestStatus::whereDate('created_at', \Carbon\Carbon::now())->where('executiveId', $user->id)->where('status', 4)->orderBy('created_at', 'DESC')->get()->unique('requestId');
                                   $serviceRequestsRejected = App\Models\ServiceRequestStatus::whereDate('created_at', \Carbon\Carbon::now())->where('executiveId', $user->id)->where('status', 4)->orderBy('created_at', 'DESC')->get()->unique('requestId');

                                   $salesRequestsCompleted = App\Models\SalesRequestStatus::where('executiveId', $user->id)->where('status', 3)->orderBy('created_at', 'DESC')->get()->unique('requestId');
                                   $serviceRequestsCompleted = App\Models\ServiceRequestStatus::where('executiveId', $user->id)->where('status', 3)->orderBy('created_at', 'DESC')->get()->unique('requestId');

                                   $salesRequestsFollowups = App\Models\SalesRequestStatus::where('executiveId', $user->id)->where('status', 2)->orderBy('created_at', 'DESC')->get()->unique('requestId');
                                   $serviceRequestsFollowups = App\Models\ServiceRequestStatus::where('executiveId', $user->id)->where('status', 2)->orderBy('created_at', 'DESC')->get()->unique('requestId');

                                   $salesRequestsInprogress = App\Models\SalesRequestStatus::where('executiveId', $user->id)->where('status', 1)->orderBy('created_at', 'DESC')->get()->unique('requestId');
                                   $serviceRequestsInprogress = App\Models\ServiceRequestStatus::where('executiveId', $user->id)->where('status', 1)->orderBy('created_at', 'DESC')->get()->unique('requestId');


                                   $todayAccepted = $salesRequestsAccepted->count() + $serviceRequestsAccepted->count();
                                   $todayRejected = $salesRequestsRejected->count() + $serviceRequestsRejected->count();
                                @endphp
                                <td>
                                    Accepted Today: {{ $todayAccepted }} <br>
                                    Rejected Today: {{ $todayRejected }} <br>
                                </td>
                                <td>
                                    Sales Request Completed: {{ $salesRequestsCompleted->count() }} <br>
                                    Service Request Completed: {{ $serviceRequestsCompleted->count() }} <br>
                                    Sales Request FollowUps: {{ $salesRequestsFollowups->count() }} <br>
                                    Service Request FollowUps: {{ $serviceRequestsFollowups->count() }} <br>
                                    Sales Request In Progress: {{ $salesRequestsInprogress->count() }} <br>
                                    Service Request In Progress: {{ $serviceRequestsInprogress->count() }} <br>
                                </td>
                            @endif
                            @if($type == 'customers')
                                <td>{{ $user->saleRequests->count() }}</td>
                                <td>{{ $user->serviceRequests->count() }}</td>
                            @endif
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop


@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css"/>
@stop

@section('js')
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>


    <script async defer>
        $(function () {
            var table = $('#usersTable').DataTable({
                buttons: [
                    'copy', 'excel', 'pdf'
                ]
            });

            new $.fn.dataTable.Buttons(table, {
                buttons: [
                    'copy', 'excel', 'pdf'
                ]
            });
        });


    </script>
@stop
