<html>
<title>Privcay Policy - KesSecurity</title>
<body>


<h2>PRIVACY POLICY</h2>
<p>•	The heart and soul of the Privacy Policy of prestigious KES securities pvt ltd, Ludhiana (Haryana) is to keep all information related with its all associated people strictly confidential in order to respect and preserve the due privacy and dignity of them all. <br>The associated bodies could be its industrial contacts, alliances and other entities.<br> Every member of our fast-progressing and responsible institution is dedicated to follow its privacy policy rigorously excepting some cases like governmental or legal ordinances and so on.</p>
<h4>TO MEET OUR ABOVE-MENTIONED OBJECTIVES, THE FOLLOWING MAIN MEASURES HAVE BEEN TAKEN BY OUR PUNCTILIOUS TEAM:</h4>

<p>•	All personal/private information and documents relating to above-noted categories of people and firms are not made open or disclosed to any third party or any outside person.</p>
<p>•	All emails, short messages or information, inquisitive queries, informational enquiries, educational guidance, faxes, letters, etc. sent ever to the company through telephonic or electronic (using internet or information technology devices) means are made inaccessible to any third party or unauthorized person/official.</p>
<p>•	All pieces of information related to pricing, financial transactions, administrative processes, personal/official discussions, etc. are also strictly maintained confidential by the company.</p>


</body>
